[![Protocol rfc1939](https://img.shields.io/badge/protocol-rfc1939-blue.svg)](https://www.ietf.org/rfc/rfc1939.txt)
[![Protocol rfc1939](https://img.shields.io/badge/rfc1939-POP3-blue.svg)](https://www.ietf.org/rfc/rfc1939.txt)
[![License](https://img.shields.io/badge/license-FreeBSD-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

[![Go v1.9](https://img.shields.io/badge/Go-v1.9-green.svg)](http://golang.org)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/gotamer/pop3)](https://goreportcard.com/report/bitbucket.org/gotamer/pop3)
[![GoDoc](https://godoc.org/bitbucket.org/gotamer/pop3?status.svg)](https://godoc.org/bitbucket.org/gotamer/pop3) 

# Post Office Protocol - Version 3 (POP3)

### Including Optional POP3 Commands:   
   - TOP  
   - UIDL  


## Example

```Go
// Create a connection to the server
c, err := pop3.DialTLS("pop.gmx.com:995", false)
if err != nil {
	log.Fatal(err)
}
defer c.Quit()

// Authenticate with the server
if err = c.Auth("username", "password"); err != nil {
	log.Fatal(err)
}

// Print the UID of all messages in the maildrop
messages, err := c.UidlAll()
if err != nil {
	log.Fatal(err)
}
for _, v := range messages {
	log.Print(v.UID)
}
```